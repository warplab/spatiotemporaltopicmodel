import collections
import itertools
from typing import Iterator


class World(collections.abc.Collection):
    '''
    A finite collection of points
    '''

    def __init__(self, points: collections.abc.Collection):
        self._points = points
        self._size = len(points)

    @property
    def points(self) -> collections.abc.Collection:
        return self._points

    @property
    def size(self) -> int:
        return self._size


    def __iter__(self) -> Iterator:
        return iter(self.points)


    def __contains__(self, __x: object) -> bool:
        return __x in self.points


    def __len__(self) -> int:
        return self.size



class IndexedWorld(World):
    '''
    A finite collection of points, each of which is associated with a unique index from each member
    of a family of index sets
    '''

    def __init__(self, index_sets: list[collections.abc.Collection]):
        indexed_points = set(itertools.product(*index_sets))
        self._shape = [len(x) for x in index_sets]
        self._dims = len(index_sets)
        super(IndexedWorld, self).__init__(indexed_points)

    @property
    def dims(self):
        return self._dims

    @property
    def shape(self):
        return self._shape
