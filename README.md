# Spatiotemporal Topic Models

Python implementation of spatiotemporal topic models.

[The source for this project is available here][src].

[Visit the WARPLab homepage for more information about what we do][warp].

[src]: https://gitlab.com/warplab/spatiotemporaltopicmodel
[warp]: https://warp.whoi.edu
