from setuptools import setup, find_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()

long_description = (here / 'README.md').read_text(encoding='utf-8')

setup(
    name='spatiotemporaltopicmodel',
    version='0.0.1',
    description='Python implementation of spatiotemporal topic models',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/warplab/spatiotemporaltopicmodel',
    author='John San Soucie',
    author_email='jsansoucie@whoi.edu',
    classifiers=[
        'Development Status :: 1 - Planning',
        'Environment :: GPU :: NVIDIA CUDA',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3 :: Only',
        'Natural Language :: English',
        'Typing :: Typed',
    ],
    keywords='topic model,bayesian,statistics,probability',
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    python_requires='>=3.8, <4',
    install_requires=[
        'torch>=1.6',
        'torchvision>=0.7',
    ],

    extras_require={
        'dev': ['check-manifest'],
        'test': ['coverage'],
    },
    entry_points={
        'console_scripts': [
            'sttopicmodel=spatiotemporaltopicmodel:main',
        ],
    },
    project_urls={
        'WARPLab homepage': 'https://warp.whoi.edu',
        'Source': 'https://gitlab.com/warplab/spatiotemporaltopicmodel',
    },
)
